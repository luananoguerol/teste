package com.votacao.app.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.votacao.app.model.Voto;
/**
 * 
 * @author luana
 *
 */
@Repository
public interface VotoRepository extends MongoRepository<Voto, String> {
 
	
	@Query("{'pauta.id' : ?0 , 'associado.id' : ?1}")
	List<Voto> findByPautaIdAndAssociadoId(String pautaId, String associadoId);
	
	@Query("{'pauta.id' : ?0 , 'opcao' : ?1}")
	List<Voto> findByPautaIdAndOpcao(String pautaId, String opcao);

}
