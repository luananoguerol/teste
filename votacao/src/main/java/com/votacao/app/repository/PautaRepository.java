package com.votacao.app.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.votacao.app.model.Pauta;
/**
 * 
 * @author luana
 *
 */
@Repository
public interface PautaRepository extends MongoRepository<Pauta, String> {
	
	public List<Pauta> findByDescricao(String descrica);

}
