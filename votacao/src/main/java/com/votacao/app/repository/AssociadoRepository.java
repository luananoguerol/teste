package com.votacao.app.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.votacao.app.model.Associado;



/**
 * 
 * @author luana
 *
 */
public interface AssociadoRepository extends MongoRepository<Associado, String> {

    public List<Associado> findByNome(String nome);
    public List<Associado> findByCpf(String cpf);
}
