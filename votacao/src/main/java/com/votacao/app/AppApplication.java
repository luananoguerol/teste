package com.votacao.app;

import org.springframework.boot.CommandLineRunner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * @author luana
 *
 */
@SpringBootApplication
public class AppApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(AppApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		//associadoRepository.save(new Associado("1", "01399735080", "Luana"));
		
	}

}
