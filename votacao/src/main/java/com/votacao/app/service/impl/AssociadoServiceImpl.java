package com.votacao.app.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.votacao.app.model.Associado;
import com.votacao.app.repository.AssociadoRepository;
import com.votacao.app.service.AssociadoService;
/**
 * 
 * @author luana
 *
 */
@Service
public class AssociadoServiceImpl implements AssociadoService{
	
	@Autowired
	private AssociadoRepository associadoRepository;
	
	public String findNextId() {
		Long count = associadoRepository.count();
		count += 1;
		return  String.valueOf(count);
	}
	
	
	
	public void save(String cpf, String nome) throws Exception{
		validateNullEmpty(cpf, "Cpf não informado.");
		validateNullEmpty(nome, "Nome não informado.");
		
		validateCpf(cpf);
		String id = this.findNextId();
		associadoRepository.save(new Associado(id, cpf, nome));
	
	}
	
	
	public void validateNullEmpty(String property, String msgError) throws Exception {
		if(property == null || property.isEmpty()) {
			throw new Exception(msgError);
		}
	}
	
	public void validateCpf(String cpf) throws Exception {
		List<Associado> list = this.findByCpf(cpf);
		if(!list.isEmpty()) {
			throw new Exception("Cpf já existente.");
		}
		
	}
	
	public Associado validateAssociado(String associadoId) throws Exception {
		Optional<Associado> associado = associadoRepository.findById(associadoId);
		if(!associado.isPresent()) {
			throw new Exception("Associado não encontrado.");
		}
		return associado.get();
	}
	
	public List<Associado> findByCpf(String cpf){
		return associadoRepository.findByCpf(cpf);
	}
	
	public List<Associado> findAll(){
		return associadoRepository.findAll();
	}
	
	public Optional<Associado> findById(String id){
		return associadoRepository.findById(id);
	}

}
