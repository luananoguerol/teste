package com.votacao.app.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.votacao.app.model.Associado;
import com.votacao.app.model.Pauta;
import com.votacao.app.model.Voto;
import com.votacao.app.repository.VotoRepository;
import com.votacao.app.service.AssociadoService;
import com.votacao.app.service.PautaService;
import com.votacao.app.service.VotoService;
/**
 * 
 * @author luana
 *
 */
@Service
public class VotoServiceImp implements VotoService{
	
	@Autowired
	private VotoRepository votoRepository;
	
	@Autowired
	private AssociadoService associadoService;
	
	@Autowired
	private PautaService pautaService;
	
	
	public String findNextId() {
		Long count = votoRepository.count();
		count += 1;
		return  String.valueOf(count);
	}
	
	
	public void save(String pautaId, String associadoId, String voto) throws Exception {
		
		validateVoto(voto);
		Pauta pauta = pautaService.validatePauta(pautaId);
		validatePautaAberta(pauta);
		Associado associado = associadoService.validateAssociado(associadoId);
		validatePautaAssociado(pautaId, associadoId);
		String id = this.findNextId();
		
		votoRepository.save(new Voto(id, pauta, associado, voto.toUpperCase()));
		
		// TODO Auto-generated method stub
		
	}
	
	public void validateVoto(String voto) throws Exception {
		
		if(voto == null || (!voto.toUpperCase().equals("S") && !voto.toUpperCase().equals("N"))) {
			throw new Exception("O voto deve ser S(Sim) ou N(Não).");			
		}
	}
	
	public void validatePautaAberta(Pauta pauta) throws Exception{
		Date dtAtual = new Date();
		if(dtAtual.before(pauta.getDataAbertura())) {
			throw new Exception("Período de votação não aberto.");			
			
		}
		if(dtAtual.after(pauta.getDataFechamento())){
			throw new Exception("Período de votação fechado.");			
		}
	}
	
	public void validatePautaAssociado(String pautaId, String associadoId) throws Exception {
		List<Voto> list = votoRepository.findByPautaIdAndAssociadoId(pautaId, associadoId);
		
		if(!list.isEmpty()) {
			throw new Exception("O associado já efetuou a votação na pauta.");
		}
	}

	
	public List<Voto> findAll(){
		return votoRepository.findAll();
	}
	
	
	public HashMap<String, Object> countVotos(String pautaId) throws Exception{
		Pauta pauta = pautaService.validatePauta(pautaId);
		return countVotos(pauta);
	}
	
	
	public HashMap<String, Object> countVotos(Pauta pauta) {
		List<Voto> listSim = votoRepository.findByPautaIdAndOpcao(pauta.getId(), "S");
		List<Voto> listNao = votoRepository.findByPautaIdAndOpcao(pauta.getId(), "N");
		
		HashMap<String, Object> map = new HashMap<>(); 
		map.put("pautaId", pauta.getId());
		map.put("descricao", pauta.getDescricao());
		map.put("S", String.valueOf(listSim.size()));
		map.put("N", String.valueOf(listNao.size()));
		
		return map;
		
	}
	

}
