package com.votacao.app.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.votacao.app.model.Pauta;
/**
 * 
 * @author luana
 *
 */
public interface PautaService {
	
	public void save(String descricao) throws Exception;
	public void save(String id, String descricao,  Date dataAbertura, Date dataFechamento) throws Exception;
	public List<Pauta> findAll();
	public Optional<Pauta> findById(String id);
	public Pauta validatePauta(String id) throws Exception;
	

}
