package com.votacao.app.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.votacao.app.model.Pauta;
import com.votacao.app.repository.PautaRepository;
import com.votacao.app.service.PautaService;
/**
 * 
 * @author luana
 *
 */
@Service
public class PautaServiceImpl implements PautaService{
	
	@Autowired
	private PautaRepository pautaRepository;
	
	public String findNextId() {
		Long count = pautaRepository.count();
		count += 1;
		return  String.valueOf(count);
	}
	
	
	
	public void save(String descricao) throws Exception{
		
		validateDescricao(descricao);
		
		Calendar dt = Calendar.getInstance();
		
		Date dataAbertura = dt.getTime();
		dt.add(Calendar.MINUTE, 1);
		
		Date dataFechamento = dt.getTime();
		
		String id = this.findNextId();

		pautaRepository.save(new Pauta(id, descricao, dataAbertura, dataFechamento ));
	
	}
	
	public void save(String id, String descricao,  Date dataAbertura, Date dataFechamento) throws Exception{
		
		validatePauta(id);
		validateDescricao(descricao);
		validateDatas(dataAbertura, dataFechamento);
		
		pautaRepository.save(new Pauta(id, descricao, dataAbertura, dataFechamento ));
		
	}
	
	
	public Pauta validatePauta(String id) throws Exception{
		Optional<Pauta> pauta = pautaRepository.findById(id);
		if(!pauta.isPresent()) {
			throw new Exception("Pauta não encontrada.");
		}
		return pauta.get();
		
	}
	
	
	public void validateDescricao(String descricao) throws Exception {
		if(descricao == null || descricao.isEmpty()) {
			throw new Exception("Descrição não informada.");
		}
	}
	
	public void validateDatas(Date dataAbertura, Date dataFechamento) throws Exception {
		
		if(dataAbertura == null) {
			throw new Exception("Data de abertura não informada.");
		}
		
		if(dataFechamento == null) {
			throw new Exception("Data de fechamento não informada.");
		}
		
		if (dataFechamento.before(dataAbertura)) {
			throw new Exception("Data de fechamento é menor que a data de abertura.");
		}
		
		if (dataFechamento.before(new Date())) {
			throw new Exception("Data de fechamento deve ser maior que a data atual.");
		}
		
	}
	
	
	public List<Pauta> findByDescricao(String descricao){
		return pautaRepository.findByDescricao(descricao);
	}
	
	
	public List<Pauta> findAll(){
		return pautaRepository.findAll();
	}
	
	public Optional<Pauta> findById(String id){
		return pautaRepository.findById(id);
	}
	

}
