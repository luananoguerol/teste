package com.votacao.app.service;

import java.util.HashMap;
import java.util.List;

import com.votacao.app.model.Voto;
/**
 * 
 * @author luana
 *
 */
public interface VotoService {
	
	public void save(String pautaId, String associadoId, String voto) throws Exception;
	public List<Voto> findAll();
	
	public HashMap<String, Object> countVotos(String pautaId) throws Exception;
	

}
