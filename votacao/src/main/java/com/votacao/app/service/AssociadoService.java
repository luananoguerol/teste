package com.votacao.app.service;

import java.util.List;
import java.util.Optional;

import com.votacao.app.model.Associado;

/**
 * 
 * @author luana
 *
 */
public interface AssociadoService {
	
	public void save(String cpf, String nome) throws Exception;
	
	public List<Associado> findAll();
	
	public List<Associado> findByCpf(String cpf);
	
	public Optional<Associado> findById(String id);
	
	public Associado validateAssociado(String associadoId) throws Exception;

}
