package com.votacao.app.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 
 * @author luana
 *
 */
@Document(collection = "associado")
public class Associado {
	
	@Id
	private String id;
	private String cpf;
	private String nome;
	
	public Associado() {
		
	}
	
	public Associado(String id, String cpf, String nome) {
		this.id = id;
		this.cpf = cpf;
		this.nome = nome;
		
	}
	

	


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	

}
