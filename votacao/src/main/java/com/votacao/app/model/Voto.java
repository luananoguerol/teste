package com.votacao.app.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
/**
 * 
 * @author luana
 *
 */
@Document(collection = "voto")
public class Voto {
	
	@Id
	private String id;
	private Pauta pauta;
	private Associado associado;
	private String opcao;
	
	
	public Voto() {
		
	}
	
	public Voto(String id, Pauta pauta, Associado associado, String opcao) {
		this.id = id;
		this.pauta = pauta;
		this.associado = associado;
		this.opcao = opcao;	
	}
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Pauta getPauta() {
		return pauta;
	}
	public void setPauta(Pauta pauta) {
		this.pauta = pauta;
	}
	public Associado getAssociado() {
		return associado;
	}
	public void setAssociado(Associado associado) {
		this.associado = associado;
	}
	public String getOpcao() {
		return opcao;
	}
	public void setOpcao(String opcao) {
		this.opcao = opcao;
	}
	
}
