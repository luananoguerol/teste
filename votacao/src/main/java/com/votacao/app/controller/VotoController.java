package com.votacao.app.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.votacao.app.model.Voto;
import com.votacao.app.service.VotoService;

/**
 * 
 * @author luana
 *
 */
@RestController
public class VotoController {
	
	
	@Autowired
	private VotoService votoService;

	@RequestMapping(
			  value = "/voto/add", 
			  params = { "pautaId", "associadoId","voto" }, 
			  method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public void add(  String pautaId, String associadoId, String voto) throws Exception {	
		votoService.save(pautaId,associadoId,voto);	
	}
	
	
	@RequestMapping("/voto/list")
	public List<Voto> buscarVotos() {
		List<Voto> list = votoService.findAll();
		return list;
	}
	
	
	@RequestMapping( value = "/voto/result", 
			  params = { "pautaId" }, 
			  method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public HashMap<String, Object> countVotos(String pautaId) throws Exception {
		HashMap<String, Object> map = votoService.countVotos(pautaId);
		return map;
	}

}
