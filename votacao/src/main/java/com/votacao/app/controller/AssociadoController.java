package com.votacao.app.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.votacao.app.model.Associado;
import com.votacao.app.service.AssociadoService;

/**
 * 
 * @author luana
 *
 */
@RestController
public class AssociadoController {
	
	
	@Autowired
	private AssociadoService associadoService;

	@RequestMapping(
			  value = "/associado/add", 
			  params = { "cpf", "nome" }, 
			  method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public void addAssociado(  String cpf, String nome) throws Exception {
		
		associadoService.save(cpf, nome);
		
	}
	
	
	@RequestMapping("/associado/list")
	public List<Associado> buscarAssociados() {
		
		List<Associado> list = associadoService.findAll();
		
		return list;	
	}

}
