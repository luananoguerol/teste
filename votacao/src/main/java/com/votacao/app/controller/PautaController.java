package com.votacao.app.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.votacao.app.model.Pauta;
import com.votacao.app.service.PautaService;

/**
 * 
 * @author luana
 *
 */
@RestController
public class PautaController {


	@Autowired
	private PautaService pautaService;

	@RequestMapping(
			value = "/pauta/add", 
			params = { "descricao" }, 
			method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public void add(String descricao) throws Exception {
		pautaService.save(descricao);

	}


	@RequestMapping("pauta/list")
	public List<Pauta> buscarPautas() {

		List<Pauta> list = pautaService.findAll();

		return list;	
	}


	@RequestMapping(
			value="pauta/update",
			params = { "id", "descricao", "dataAbertura", "dataFechamento" }, 
			method = RequestMethod.POST)
	public void update(String id, String descricao, String dataAbertura, String dataFechamento) throws ParseException, Exception{

		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy"); 
		Date dtAbertura = format.parse(dataAbertura);

		Date dtFechamento = format.parse(dataFechamento);

		pautaService.save(id, descricao, dtAbertura, dtFechamento);

	}

}
