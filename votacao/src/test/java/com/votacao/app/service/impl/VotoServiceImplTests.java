package com.votacao.app.service.impl;


import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.votacao.app.model.Associado;
import com.votacao.app.model.Pauta;
import com.votacao.app.model.Voto;
import com.votacao.app.repository.VotoRepository;
import com.votacao.app.service.AssociadoService;
import com.votacao.app.service.PautaService;
/**
 * 
 * @author luana
 *
 */
@SpringBootTest
class VotoServiceImplTests {
	
	@MockBean
	private PautaService pautaService;
	
	@MockBean
	private AssociadoService associadoService;
	
	@MockBean
	private VotoRepository votoRepository;
	
	@Autowired
	private VotoServiceImp votoServiceImp;
	
	@Test
	void validateVotoNull(){	
		
		try {
			votoServiceImp.validateVoto(null);
			fail();
		} catch (Exception e) {
			assertTrue(e.getMessage().equals("O voto deve ser S(Sim) ou N(Não)."));
		}	
	}
	
	@Test
	void validateVotoInvalido(){	
		
		try {
			votoServiceImp.validateVoto("F");
			fail();
		} catch (Exception e) {
			assertTrue(e.getMessage().equals("O voto deve ser S(Sim) ou N(Não)."));
		}	
	}
	
	@Test
	void validateVotoSim(){	
		
		try {
			votoServiceImp.validateVoto("S");	
		} catch (Exception e) {
			fail();
			e.printStackTrace();
		}	
	}
	
	@Test
	void validateVotoNao(){	
		
		try {
			votoServiceImp.validateVoto("N");	
		} catch (Exception e) {
			fail();
			e.printStackTrace();
		}	
	}
	
	
	@Test
	void validatePautaAssociadoNaoEncontrado(){
		
		String pautaId = "123";
		String associadoId = "456";
		
		List<Voto> list = new ArrayList<Voto>();
		
		when(votoRepository.findByPautaIdAndAssociadoId(pautaId, associadoId)).thenReturn(list);
		
		try {
			votoServiceImp.validatePautaAssociado(pautaId, associadoId);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}	
	}
	
	@Test
	void validatePautaAssociadoEncontrado(){
		
		String pautaId = "123";
		String associadoId = "456";
		
		List<Voto> list = new ArrayList<Voto>();
		list.add(new Voto());
		
		when(votoRepository.findByPautaIdAndAssociadoId(pautaId, associadoId)).thenReturn(list);
		
		try {
			votoServiceImp.validatePautaAssociado(pautaId, associadoId);
			fail();
		} catch (Exception e) {
			assertTrue(e.getMessage().equals("O associado já efetuou a votação na pauta."));
			
		}	
	}
	
	@Test
	void validatePautaAberta(){
		
		Calendar cAbertura = Calendar.getInstance();
		Calendar cFechamento = Calendar.getInstance();
		
		cAbertura.set(Calendar.MONTH, cAbertura.get(Calendar.MONTH)-1);
		cFechamento.set(Calendar.MONTH, cFechamento.get(Calendar.MONTH)+1);
		
		Pauta pauta = new Pauta("1", "Pauta 1", cAbertura.getTime(), cFechamento.getTime());
		
		try {
			votoServiceImp.validatePautaAberta(pauta);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
			
		}	
	}
	
	@Test
	void validatePautaDataVotoMenorQueDataAbertura(){
		
		Calendar cAbertura = Calendar.getInstance();
		Calendar cFechamento = Calendar.getInstance();
		
		cAbertura.set(Calendar.MONTH, cAbertura.get(Calendar.MONTH)+1);
		cFechamento.set(Calendar.MONTH, cFechamento.get(Calendar.MONTH)+2);
		
		Pauta pauta = new Pauta("1", "Pauta 1", cAbertura.getTime(), cFechamento.getTime());
		
		try {
			votoServiceImp.validatePautaAberta(pauta);
			fail();
		} catch (Exception e) {
			assertTrue(e.getMessage().equals("Período de votação não aberto."));
			
		}	
	}
	
	@Test
	void validatePautaDataVotoMaiorQueDataFechamento(){
		
		Calendar cAbertura = Calendar.getInstance();
		Calendar cFechamento = Calendar.getInstance();
		
		cAbertura.set(Calendar.MONTH, cAbertura.get(Calendar.MONTH)-2);
		cFechamento.set(Calendar.MONTH, cFechamento.get(Calendar.MONTH)-1);
		
		Pauta pauta = new Pauta("1", "Pauta 1", cAbertura.getTime(), cFechamento.getTime());
		
		try {
			votoServiceImp.validatePautaAberta(pauta);
			fail();
		} catch (Exception e) {
			assertTrue(e.getMessage().equals("Período de votação fechado."));
			
		}	
	}
	
	@Test
	void save(){
		
		String pautaId = "123";
		String associadoId = "456";
		
		List<Voto> list = new ArrayList<Voto>();
		
		Calendar cAbertura = Calendar.getInstance();
		Calendar cFechamento = Calendar.getInstance();
		
		cAbertura.set(Calendar.MONTH, cAbertura.get(Calendar.MONTH)-1);
		cFechamento.set(Calendar.MONTH, cFechamento.get(Calendar.MONTH)+1);
		
		Optional<Pauta> pauta = Optional.of(new Pauta("1", "Pauta 1", cAbertura.getTime(), cFechamento.getTime()));
		Optional<Associado> associado = Optional.of(new Associado());
		
		try {
			when(pautaService.validatePauta(pautaId)).thenReturn(pauta.get());
			when(associadoService.validateAssociado(associadoId)).thenReturn(associado.get());
			when(votoRepository.findByPautaIdAndAssociadoId(pautaId, associadoId)).thenReturn(list);
			
			votoServiceImp.save(pautaId, associadoId, "S");
			
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}	
	}
	
	@Test
	void countVotos(){
		
		String pautaId = "123";
		
		Optional<Pauta> pauta = Optional.of(new Pauta());
		
		try {
			when(pautaService.validatePauta(pautaId)).thenReturn(pauta.get());
		
			votoServiceImp.countVotos(pautaId);
			
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}	
	}
	
}
