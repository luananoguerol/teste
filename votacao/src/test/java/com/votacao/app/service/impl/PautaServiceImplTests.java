package com.votacao.app.service.impl;


import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.votacao.app.model.Pauta;
import com.votacao.app.repository.PautaRepository;
/**
 * 
 * @author luana
 *
 */
@SpringBootTest
class PautaServiceImplTests {
	
	@MockBean
	private PautaRepository pautaRepository;
	
	@Autowired
	private PautaServiceImpl pautaServiceImpl;
	
	@Test
	void validateDescricaoNaoInformada(){	
		
		try {
			pautaServiceImpl.validateDescricao(null);
			fail();
		} catch (Exception e) {
			assertTrue(e.getMessage().equals("Descrição não informada."));
		}	
	}
	
	@Test
	void validateDescricaoVazia(){	
		
		try {
			pautaServiceImpl.validateDescricao("");
			fail();
		} catch (Exception e) {
			assertTrue(e.getMessage().equals("Descrição não informada."));
		}
		
	}
	
	@Test
	void validateDataAberturaNull(){	
		
		try {
			pautaServiceImpl.validateDatas(null, new Date());
			fail();
		} catch (Exception e) {
			assertTrue(e.getMessage().equals("Data de abertura não informada."));
		}
		
	}
	
	@Test
	void validateDataFechamentoNull(){	
		
		try {
			pautaServiceImpl.validateDatas(new Date(), null);
			fail();
		} catch (Exception e) {
			assertTrue(e.getMessage().equals("Data de fechamento não informada."));
		}
		
	}
	
	
	@Test
	void validateDataFechamentoMenorQueDataAbertura(){	
		
		Calendar c = Calendar.getInstance();
		
		c.set(Calendar.YEAR, c.get(Calendar.YEAR)-1);
				
		try {
			pautaServiceImpl.validateDatas(new Date(), c.getTime());
			fail();
		} catch (Exception e) {
			assertTrue(e.getMessage().equals("Data de fechamento é menor que a data de abertura."));
		}
		
	}
	
	
	@Test
	void validateDataFechamentoMenorQueDataAtual(){	
		
		Calendar cAbertura = Calendar.getInstance();
		Calendar cFechamento = Calendar.getInstance();
		
		cAbertura.set(Calendar.MONTH, cAbertura.get(Calendar.MONTH)-1);
		cFechamento.set(Calendar.WEEK_OF_MONTH, cFechamento.get(Calendar.WEEK_OF_MONTH)-1);
			
		try {
			pautaServiceImpl.validateDatas(cAbertura.getTime(), cFechamento.getTime());
			fail();
		} catch (Exception e) {
			assertTrue(e.getMessage().equals("Data de fechamento deve ser maior que a data atual."));
		}
		
	}
	
	@Test
	void validatePautaNaoEncontrada(){
		
		String pautaId = "123";
		
		Optional<Pauta> p = Optional.empty();
		
		when(pautaRepository.findById(pautaId)).thenReturn(p);
		
		try {
			pautaServiceImpl.validatePauta(pautaId);
			fail();
		} catch (Exception e) {
			assertTrue(e.getMessage().equals("Pauta não encontrada."));
		}	
	}
	
	
	@Test
	void validatePautaEncontrada(){
		
		String pautaId = "123";
		
		Optional<Pauta> p = Optional.of(new Pauta());
		
		when(pautaRepository.findById(pautaId)).thenReturn(p);
		
		try {
			pautaServiceImpl.validatePauta(pautaId);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}	
	}
	
	
	@Test
	void savePautaDescricao(){
		try {
			pautaServiceImpl.save("Pauta 1");
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	void savePauta(){
		
		String pautaId = "123";
		Optional<Pauta> p = Optional.of(new Pauta());
		
		Calendar cFechamento = Calendar.getInstance();
		cFechamento.set(Calendar.DATE, cFechamento.get(Calendar.DATE)+1);
		
		when(pautaRepository.findById(pautaId)).thenReturn(p);
		try {
			pautaServiceImpl.save(pautaId, "Pauta 2", new Date(), cFechamento.getTime());
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}	

}
