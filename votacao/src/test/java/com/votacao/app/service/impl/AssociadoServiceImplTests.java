package com.votacao.app.service.impl;


import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.votacao.app.model.Associado;
import com.votacao.app.repository.AssociadoRepository;
/**
 * 
 * @author luana
 *
 */
@SpringBootTest
class AssociadoServiceImplTests {
	
	@MockBean
	private AssociadoRepository associadoRepository;
	
	@Autowired
	private AssociadoServiceImpl associadoServiceImpl;
	
	@Test
	void validateCpfCadastrado(){
		
		String cpf = "30850404045";
		
		List<Associado> list = new ArrayList<Associado>();
		list.add(new Associado());
		
		when(associadoRepository.findByCpf(cpf)).thenReturn(list);
		
		try {
			associadoServiceImpl.validateCpf(cpf);
			fail();
		} catch (Exception e) {
			assertTrue(e.getMessage().equals("Cpf já existente."));
		}
		
	}
	
	
	@Test
	void validateCpfNaoCadastrado(){
		String cpf = "30850404045";
		
		when(associadoRepository.findByCpf(cpf)).thenReturn(Collections.emptyList());
		try {
			associadoServiceImpl.validateCpf(cpf);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}	
	}
	
	
	
	@Test
	void validateSave() {
		String cpf = "30850404045";
		String nome = "José da Silva";
		
		when(associadoRepository.findByCpf(cpf)).thenReturn(Collections.emptyList());
		try {
			associadoServiceImpl.save(cpf, nome);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}	
	}
	
	
	@Test
	void validateSaveCpfCadastrado(){
		
		String cpf = "30850404045";
		String nome = "José da Silva";
		
		List<Associado> list = new ArrayList<Associado>();
		list.add(new Associado());
		
		when(associadoRepository.findByCpf(cpf)).thenReturn(list);
		
		try {
			associadoServiceImpl.save(cpf, nome);
			fail();
		} catch (Exception e) {
			assertTrue(e.getMessage().equals("Cpf já existente."));
		}	
	}

	
	@Test
	void validateSaveNomeNull(){
		String cpf = "30850404045";
		
		when(associadoRepository.findByCpf(cpf)).thenReturn(Collections.emptyList());
		
		try {
			associadoServiceImpl.save(cpf, null);
			fail();
		} catch (Exception e) {
			assertTrue(e.getMessage().equals("Nome não informado."));
		}	
		
	}
	
	
	@Test
	void validateAssociadoNaoEncontrada(){
		
		String associadoId = "123";
		
		Optional<Associado> a = Optional.empty();
		
		when(associadoRepository.findById(associadoId)).thenReturn(a);
		
		try {
			associadoServiceImpl.validateAssociado(associadoId);
			fail();
		} catch (Exception e) {
			assertTrue(e.getMessage().equals("Associado não encontrado."));
		}	
	}
	
	
	
	@Test
	void validateAssociadoNaoEncontrado(){
		
		String associadoId = "123";
		
		Optional<Associado> a = Optional.empty();
		
		when(associadoRepository.findById(associadoId)).thenReturn(a);
		
		try {
			associadoServiceImpl.validateAssociado(associadoId);
			fail();
		} catch (Exception e) {
			assertTrue(e.getMessage().equals("Associado não encontrado."));
		}	
	}
	
	
	@Test
	void validateAssociadoEncontrado(){
		
		String associadoId = "123";
		
		Optional<Associado> a = Optional.of(new Associado());
		
		when(associadoRepository.findById(associadoId)).thenReturn(a);
		
		try {
			associadoServiceImpl.validateAssociado(associadoId);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}	
	}

}
