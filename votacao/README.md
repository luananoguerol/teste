# Getting Started

### Para inicializar a aplicação
Utilizar SpringBoot

### Para configurar banco de dados
Para a aplicação foi utilizado o banco MongoDB, na https://cloud.mongodb.com/, porém, pode ser utilizado um banco MongoDB local. Basta efetuar a configuração do banco no arquivo application.properties

### Rest para inserir Associados 
http://localhost:8080/associado/add

### Rest para listar Associados 
http://localhost:8080/associado/list

### Rest para inserir pauta
http://localhost:8080/pauta/add

### Rest para listar pauta
http://localhost:8080/pauta/list

### Rest para dar update da pauta, informando data de abertura e fechamento
http://localhost:8080/pauta/update

### Rest para inserir voto
http://localhost:8080/voto/add

### Rest para listar voto
http://localhost:8080/voto/list

### Rest para apresentar resultado da votação
http://localhost:8080/voto/result?pautaId=<pautaId>


### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.2.2.RELEASE/maven-plugin/)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.2.2.RELEASE/reference/htmlsingle/#boot-features-developing-web-applications)
* [Spring Data MongoDB](https://docs.spring.io/spring-boot/docs/2.2.2.RELEASE/reference/htmlsingle/#boot-features-mongodb)

### Guides
The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)
* [Accessing Data with MongoDB](https://spring.io/guides/gs/accessing-data-mongodb/)

